/**
 * @author Yinghan Wang <wyhpractice@gmail.com>
 * @date 17/11/2020
 */
const util = require('./util');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

function DataGen() {
    /**
     * @config
     * when anomoly occours it will continue occuring
     * for a numbe of seconds. In this case we set the
     * min value to 50
     */
    const MIN_ANOMOLY_UNITS = 50;

    /**
     * @config
     * when anomoly occours it will continue occuring
     * for a numbe of seconds. In this case we set the
     * max value to 80
     */
    const MAX_ANOMOLY_UNITS = 80;

    /**
     * @config
     * the number of records to create
     */
    const NUM_OF_RECORD = 10000;

    /**
     * @config
     * the probability of an anomoly to occour
     */
    const ASSSUMED_PROBABILITY = 0.005;

    /**
     * @config
     * KPI value range in case of anomoly
     */
    const LOW_KPI_RANGE = [98, 99.1];

    /**
     * @config
     * KPI value range in case of non-anomoly
     */
    const HI_KPI_RANGE = [99.3, 100];

    /**
     * @config
     * Time is global
     */
    let TIME = new Date().getTime();

    /**
     * csvWriter instance with destination path and the
     * header pre-defined
     */
    const csvWriter = createCsvWriter({
        path: './data/file.csv',
        header: [
            {id: 'kpi', title: 'kpi'},
            {id: 'timeStamp', title: 'timeStamp'},
            {id: 'anomoly', title: 'anomoly'},
        ]
    });

    /**
     * @record { Array }
     * @return null
     * Create csv file with the records
     */
    const createCSVFile = async (records) => {
        await csvWriter.writeRecords(records);
        console.log('Done! Check ./data/file.csv');
    }

    /**
     * calculate anomoly binary value based on
     * the predefined probability
     */
    const calAnomoly = () => Math.random() < ASSSUMED_PROBABILITY ? 1 : 0;



    /**
     * @return {Object}
     */
    const genRegularRecord = () => ({
        anomoly: 0, timeStamp: ++TIME, 
        kpi: util.getRandom.apply(null, HI_KPI_RANGE) / 100,
    });

    /**
     * @return {Array} data
     * generate records in case of anomoly
     */
    const genAnomolyRecords = () => {
        let continuity = Math.floor(util.getRandom(MIN_ANOMOLY_UNITS, MAX_ANOMOLY_UNITS));
        let data = [];
        while(continuity--) {
            data = [...data, {
                anomoly: 1,timeStamp: ++TIME, 
                kpi: util.getRandom.apply(null, LOW_KPI_RANGE) / 100, 
            }];
        }
        return data;
    }

    /**
     * @return {Array} - data
     * start generating the data
     */
    const genData = () => {
        let data = [];
        while(true) {
            if(data.length >= NUM_OF_RECORD) break;
            if(calAnomoly() === 1) data = [...data, ...genAnomolyRecords()];
            else data = [...data, genRegularRecord()];
        }
        return data;
    };

    /**
     * APIs
     */
    return {
        genData,
        createCSVFile,
    }
}

module.exports = DataGen;