/**
 * @author Yinghan Wang
 */

// import DataGen
const DataGen = require('./lib/data-generator');

// create DataGen instance
const dg = DataGen();

// create data
const data = dg.genData();

// write it into a csv file
dg.createCSVFile(data);